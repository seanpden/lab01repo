package cs.mad.flashcards

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardSet

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        println("THE FOLLOWING ARE TESTS:\n-------------\n")

        val testFlash = Flashcard("Foobar Term", "Foobar Def")
        println(testFlash.cardCollection())
        println(testFlash)
        val testFlashSet = FlashcardSet("Foobar Title")
        println(testFlashSet.setCollection())
        println(testFlashSet)

        println("-------------\nEND OF TESTS\n")

    }
}