package cs.mad.flashcards.entities

data class FlashcardSet(var Title: String) {

    fun setCollection(): List<FlashcardSet> {

        val setofFlash00 = FlashcardSet("This is a title")
        val setofFlash01 = FlashcardSet("This is a title")
        val setofFlash02 = FlashcardSet("This is a title")
        val setofFlash03 = FlashcardSet("This is a title")
        val setofFlash04 = FlashcardSet("This is a title")
        val setofFlash05 = FlashcardSet("This is a title")
        val setofFlash06 = FlashcardSet("This is a title")
        val setofFlash07 = FlashcardSet("This is a title")
        val setofFlash08 = FlashcardSet("This is a title")
        val setofFlash09 = FlashcardSet("This is a title")

        return listOf(
            setofFlash00,
            setofFlash01,
            setofFlash02,
            setofFlash03,
            setofFlash04,
            setofFlash05,
            setofFlash06,
            setofFlash07,
            setofFlash08,
            setofFlash09
        )

    }

}