package cs.mad.flashcards.entities

data class Flashcard(var Term: String, var Definition: String) {

    fun cardCollection(): List<Flashcard> {

        val flash00 = Flashcard("This is a Term", "This is a Definition")
        val flash01 = Flashcard("This is a Term", "This is a Definition")
        val flash02 = Flashcard("This is a Term", "This is a Definition")
        val flash03 = Flashcard("This is a Term", "This is a Definition")
        val flash04 = Flashcard("This is a Term", "This is a Definition")
        val flash05 = Flashcard("This is a Term", "This is a Definition")
        val flash06 = Flashcard("This is a Term", "This is a Definition")
        val flash07 = Flashcard("This is a Term", "This is a Definition")
        val flash08 = Flashcard("This is a Term", "This is a Definition")
        val flash09 = Flashcard("This is a Term", "This is a Definition")


        return listOf(
            flash00,
            flash01,
            flash02,
            flash03,
            flash04,
            flash05,
            flash06,
            flash07,
            flash08,
            flash09
        )

    }

}
